require 'adal'
require 'tb_core'

module TbAzure
  class Engine < ::Rails::Engine
    isolate_namespace TbAzure

    config.generators do |g|
      g.test_framework :rspec, fixture: false
      g.fixture_replacement :factory_girl, dir: 'spec/factories'
      g.assets false
      g.helper true
    end

    initializer 'tb_azure.adal_settings' do
      ADAL::Logging.log_level = if Rails.env.development?
                                  # See https://github.com/AzureAD/azure-activedirectory-library-for-ruby/pull/46
                                  defined?(ADAL::ADLogger) ? ADAL::ADLogger::VERBOSE : ADAL::Logger::VERBOSE
                                else
                                  defined?(ADAL::ADLogger) ? ADAL::ADLogger::INFO : ADAL::Logger::INFO
                                end
      ADAL::Logging.log_output = Rails.root.join('log', 'adal.log')
    end

    # initializer 'tb_azure.models' do
    config.after_initialize do
      ActiveSupport.on_load(:active_record) do
        SpudUser.send :include, TbAzure::User
      end
    end

  end
end
