module TbAzure
  include ActiveSupport::Configurable
  config_accessor :tenant, :client_id, :secret, :refresh_interval, :mapper, :user_attrs

  # Get defaults fron environment variables
  self.client_id = ENV['AZURE_CLIENT_ID']
  self.secret = ENV['AZURE_SECRET']
  self.tenant = ENV['AZURE_TENANT']
  self.mapper = nil
  self.user_attrs = nil

  # How often should we check in with the azure service
  self.refresh_interval = 1.minute
end
