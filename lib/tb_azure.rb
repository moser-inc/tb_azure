require 'tb_azure/engine'

module TbAzure
  require 'tb_azure/configuration'

  RESOURCE = 'https://graph.microsoft.com'.freeze
  AUTHORITY = 'login.microsoftonline.com'.freeze
  API_VERSION = 'v1.0'.freeze

  module ClassMethods
    def context
      ADAL::AuthenticationContext.new(AUTHORITY, TbAzure.tenant)
    end

    def client
      ADAL::ClientCredential.new(TbAzure.client_id, TbAzure.secret)
    end

    def user_attribute_params
      user_attrs = %w[id displayName mail surname givenName]
      user_attrs += TbAzure.user_attrs if TbAzure.user_attrs
      user_attrs_params = {
        '$select': user_attrs.join(',')
      }.freeze
      user_attrs_params
    end
  end
  extend ClassMethods
end
