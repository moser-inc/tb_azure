class CreateTbAzureTokens < ActiveRecord::Migration[5.0]
  def change
    create_table :tb_azure_tokens do |t|
      t.references :spud_user, index: true, foreign_key: true, null: false
      t.string :access_token, null: false
      t.string :refresh_token, null: false
      t.timestamps null: false
      t.timestamps
    end
  end
end
