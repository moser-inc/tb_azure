class AddAzureUpdatedAtToSpudUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :spud_users, :azure_updated_at, :datetime
  end
end
