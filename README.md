# TbAzure

This engine enables Azure AD users to log in to your [tb_core](https://bitbucket.org/moser-inc/tb_core) based Rails application.

## Known Issue

There is a compatibility problem with the current version of `adal` and Rails. See [pull request #46](https://github.com/AzureAD/azure-activedirectory-library-for-ruby/pull/46) for more info.

For now, you need to use ruby 2.3.1 or [this fork of adal](https://github.com/eavonius/azure-activedirectory-library-for-ruby).

## Requirements

This gem expects that you have a working Azure AD tenant. Here are some resources to get you started:

- [Azure Active Directory developer's guide](https://azure.microsoft.com/en-us/documentation/articles/active-directory-developers-guide/)
- [How to get an Azure Active Directory tenant](https://azure.microsoft.com/en-us/documentation/articles/active-directory-howto-tenant/)

If you're not sure what any of this means, ask your local IT person or AD admin.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tb_azure'
```

And then execute:
```bash
$ bundle
```

Install and run the database migrations:

```bash
rails railties:install:migrations
rails db:migrate
```

## Usage

Configure your tenant, client, and secret. This will configure how you log in to Azure AD.

```ruby
TbAzure.configure do |config|
  config.client_id = 'client'
  config.secret = 'secret'
  config.tenant = 'organization.com'
  config.refresh_interval = 1.minute
end
```

Or alternatively, set the values via environment variables.

```bash
export AZURE_CLIENT_ID=''
export AZURE_SECRET=''
export AZURE_TENANT=''
```

Mount the engine in our application's `routes.rb` file. This will determine the base path for azure login.

```ruby
Rails.application.routes.draw do
  mount TbAzure::Engine => "/azure"
end
```

Add a link somewhere in your application to the login path.

```erb
<p>
  <%= link_to 'Log in with Azure', tb_azure.login_path %>
</p>
```

You can add optional params to the azure login link.

- If `return_to` is passed, that path will be used when redirecting instead of root
- All other params are passed through as get params to the redirect path

```erb
<p>
  <%= link_to 'Log in with Azure', tb_azure.login_path(test: 'Hello World', return_to: 'https://myapp.com/some/path') %>
</p>
```

The above would result in the following redirect url: `https://myapp.com/some/path?test=Hello%20World`

## Controller Module

An optional module can be included in your Rails controllers.

```
class ApplicationController < ActionController::Base
  prepend TbAzure::AzureAuthentication
end
```

Including this module will cause users to be validated against Azure AD at an interval controlled by `TbAzure.refresh_interval`.

## Azure AD REST API

Once you have an azure token you can start making http requests.

```ruby
# Get the user's profile
token = TbAzure::Token.first
json = token.request('/me')
```

See [Microsoft Graph API REST](https://developer.microsoft.com/en-us/graph/docs) for more information.

## Useful Operations

### Find or create a user from an azure profile

Assuming `profile` is a json object you got from the Azure AD graph, you can pass it to the `FindOrCreate` operation to create a user. Optionally accepts an `access_token` object, which can be used to fetch additional attributes.

```ruby
TbAzure::User::FindOrCreate.run(profile, access_token: token)
```

### Create a user from an azure token

Similar to the above, but instead of accepting a user profile it takes a token response object.

```ruby
TbAzure::Token::Login.run(token_response)
```

### Refresh an access token

Access tokens are usually refreshed automatically, but sometimes you might need to do so manually.

```ruby
TbAzure::Token::Refresh.run(access_token)
```

## Attribute Mapping

The `FindOrCreate` operation will automatically map commonly used attributes like `first_name`, `last_name`, and `email` to the local spud user object. Sometimes you might want to map additional information. You can do that using the `TbAzure.mapper` option.

- Add the desired attributes to your spud user table.

```
bin/rails g migration add_job_title_to_spud_users job_title:string
bin/rake db:migrate
```

- Create a mapper class. This is a plain ruby object that must implement a `#map_attributes` method. The method takes a `profile` and a `token`. It must return a hash, which will be merged into the resulting `SpudUser`.
- The below example pulls the `jobTitle` attribute out of the azure profile and assigns it to a `job_title` property on spud user.

```ruby
class AzureAttributeMapper
  def map_attributes(profile, token)
    { job_title: profile['jobTitle'] }
  end
end
```

Take note that the `token` is an optional argument and might be nil.

- Configure the mapper.

```ruby
TbAzure.configure do |config|
  config.mapper = 'AzureAttributeMapper'
end
```

Here is a more complex mapper example that uses the token to fetch even more info about the user. Be sure to rescue from network errors where appropriate.

```ruby
class AzureAttributeMapper
  def map_attributes(profile, token)
    {
      job_title: profile['jobTitle'],
      manager: fetch_manager(profile['objectId'], token)
    }
  end

  def fetch_manager(azure_id, token)
    result = token.request("/users/#{azure_id}/manager")
    result['mail']
  rescue TbAzure::Network::RequestError
    nil
  end
end
```

The default user attributes that will be returned are id, displayName, mail, surname, givenName
- if you need to add aditional attributes add the following to the config
- configure the user attributes needed 

```ruby
TbAzure.configure do |config|
  config.user_attrs = %w(businessPhones jobTitle mobilePhone accountEnabled)
end
```