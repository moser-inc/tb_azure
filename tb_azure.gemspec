$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'tb_azure/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = 'tb_azure'
  s.version     = TbAzure::VERSION
  s.authors     = ['Greg Woods']
  s.email       = ['greg.woods@moserit.com']
  s.homepage    = 'https://bitbucket.org/moserit/tb_redirects'
  s.summary     = 'Support for Azure AD login with Twice Baked'
  s.description = 'Let your users log in to a twice baked Rails application using their AD login'
  s.license     = 'MIT'

  s.files = Dir['{app,config,db,lib}/**/*'] + ['MIT-LICENSE', 'Rakefile', 'README.md']
  s.test_files = Dir.glob('spec/**/*').reject { |f| f.match(%r{^spec\/dummy\/(log|tmp)}) }

  s.add_dependency 'adal', '>= 1.0.0'
  s.add_dependency 'rails', '>= 5.0.0.1'
  s.add_dependency 'tb_core', '>= 1.4.7'

  s.add_development_dependency 'database_cleaner'
  s.add_development_dependency 'factory_bot_rails'
  s.add_development_dependency 'pg', '>= 0.15'
  s.add_development_dependency 'rspec-rails'
  s.add_development_dependency 'rubocop', '0.63.1'
  s.add_development_dependency 'simplecov'
  s.add_development_dependency 'webmock'
end
