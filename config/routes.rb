TbAzure::Engine.routes.draw do
  get 'login' => 'user_sessions#new'
  post 'callback' => 'user_sessions#callback'
end
