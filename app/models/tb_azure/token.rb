module TbAzure
  class Token < ApplicationRecord
    belongs_to :spud_user

    def profile
      request('/me')
    end

    def refresh!
      Refresh.run(self)
    end

    # Perform a network request. If the request fails due to an
    # expired access token, refresh the token and try again automatically
    #
    def request(endpoint, params = {})
      Network.request(endpoint, access_token, params)
    rescue Network::ExpiredTokenError
      refresh!
      Network.request(endpoint, access_token, params)
    end
  end
end
