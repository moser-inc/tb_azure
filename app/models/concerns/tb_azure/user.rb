module TbAzure
  module User
    extend ActiveSupport::Concern

    included do
      has_one :azure_token,
        dependent: :destroy,
        class_name: 'TbAzure::Token'
    end

    def azure?
      azure_principal_name.present? || azure_object_id.present?
    end

    def should_check_azure?
      return false unless azure?
      return false unless TbAzure.refresh_interval

      azure_updated_at.nil? || azure_updated_at < TbAzure.refresh_interval.ago
    end
  end
end
