module TbAzure
  module Network
    module ClassMethods
      # Perform a GET request
      #
      def request(endpoint_or_url, access_token, params = {})
        Request.new(access_token).request(endpoint_or_url, params)
      end
    end
    extend ClassMethods
  end
end
