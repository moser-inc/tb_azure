module TbAzure
  module Network
    class Request

      def initialize(access_token)
        @access_token = access_token
      end

      # Perform a GET request
      #
      def request(endpoint_or_url, params = nil)
        resource_uri = uri_for_endpoint(endpoint_or_url)
        resource_uri.query = URI.encode_www_form(params) if params.present?
        http = Net::HTTP.new(resource_uri.hostname, resource_uri.port)
        http.use_ssl = true
        body = http.get(resource_uri, authorization: @access_token).body
        json = JSON.parse(body)
        validate_json(json)
        json
      end

      # Get the full URI for an endpoint
      #
      def uri_for_endpoint(endpoint_or_url)
        if /^https/.match?(endpoint_or_url)
          URI(endpoint_or_url)
        else
          URI(TbAzure::RESOURCE + '/' + TbAzure::API_VERSION + endpoint_or_url)
        end
      end

      private

      def validate_json(json)
        error = json['error']
        return if error.blank?
        raise ExpiredTokenError, error if error['code'] == 'InvalidAuthenticationToken'

        raise RequestError, error
      end
    end
  end
end
