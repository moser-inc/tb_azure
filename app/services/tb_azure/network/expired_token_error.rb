module TbAzure
  module Network
    class ExpiredTokenError < RequestError
    end
  end
end
