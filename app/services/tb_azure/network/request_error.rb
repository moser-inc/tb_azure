module TbAzure
  module Network
    class RequestError < StandardError
      attr_reader :code
      def initialize(json)
        @code = json['code']
        @value = json['message'].try(:[], 'value')
        super("#{@code}: #{@value}")
      end
    end
  end
end
