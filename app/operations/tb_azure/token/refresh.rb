module TbAzure
  class Token
    class Refresh < TbAzure::Operation

      class RefreshError < StandardError
      end

      # Refresh the access token
      #
      def call(azure_token)
        token_response = TbAzure.context.acquire_token_with_refresh_token(
          azure_token.refresh_token,
          TbAzure.client,
          TbAzure::RESOURCE
        )
        case token_response
        when ADAL::SuccessResponse
          azure_token.update(
            access_token: token_response.access_token,
            refresh_token: token_response.refresh_token
          )
        when ADAL::ErrorResponse
          raise RefreshError, token_response.error_description
        end
      end
    end
  end
end
