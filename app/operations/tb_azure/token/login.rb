module TbAzure
  class Token
    class Login < TbAzure::Operation

      # Log the user in with an azure token response object
      #
      def call(token_response)
        profile = TbAzure::Network.request(
          '/me',
          token_response.access_token,
          TbAzure.user_attribute_params
        )
        token = new_azure_token(token_response)
        user = User::FindOrCreate.run(
          profile,
          access_token: token
        ).result
        persist_token(user, token) unless user.errors.any?
        user
      end

      private

      # Build an unpersisted token
      #
      def new_azure_token(response)
        TbAzure::Token.new(
          access_token: response.access_token,
          refresh_token: response.refresh_token
        )
      end

      # Persist the token to the database
      # If one already exists for the user, replace it
      #
      def persist_token(user, token)
        token_attrs = {
          access_token: token.access_token,
          refresh_token: token.refresh_token
        }
        if user.azure_token
          user.azure_token.update(token_attrs)
        else
          user.create_azure_token(token_attrs)
        end
      end

    end
  end
end
