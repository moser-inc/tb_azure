module TbAzure
  module User
    class Refresh < TbAzure::Operation
      include TbAzure::User::Helpers

      def call(user, azure_token)
        raise ArgumentError, 'Attempting to refresh user with nil azure token' unless azure_token

        profile = azure_token.request(
          "/users/#{user.azure_object_id}",
          TbAzure.user_attribute_params
        )
        user.update(azure_user_attributes(profile, azure_token))
      end
    end
  end
end
