module TbAzure
  module User
    module Helpers

      # Translate profile json data into attributes
      # for a user model
      #
      def azure_user_attributes(profile, access_token)
        azure_attrs = {
          azure_object_id: profile['id'],
          azure_principal_name: profile['displayName'],
          azure_updated_at: Time.zone.now,
          login: profile['mail'],
          email: profile['mail'],
          first_name: profile['givenName'],
          last_name: profile['surname']
        }
        azure_attrs.merge!(additional_mapping(profile, access_token)) if TbAzure.mapper
        azure_attrs
      end

      def additional_mapping(profile, access_token)
        mapper = Object.const_get(TbAzure.mapper).new
        mapper.map_attributes(profile, access_token)
      end

    end
  end
end
