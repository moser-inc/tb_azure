module TbAzure
  module User
    class FindOrCreate < TbAzure::Operation
      include TbAzure::User::Helpers

      # Create a user from a profile JSON
      #
      def call(profile, access_token: nil)
        user = SpudUser.find_by(azure_object_id: profile['id'])
        user = build_user if user.blank?
        user.assign_attributes(azure_user_attributes(profile, access_token))
        handle_duplicate_email(user)

        user.save
        user
      end

      private

      # Build a user with randomized password
      #
      def build_user
        SpudUser.new do |u|
          random_pass = SecureRandom.hex
          u.password = random_pass
          u.password_confirmation = random_pass
        end
      end

      def handle_duplicate_email(user)
        duplicate = SpudUser.where(email: user.email)
                            .where.not(id: user.id)
                            .first

        return unless duplicate

        new_value = [Time.now.to_i, user.email].join('-')

        duplicate.update(
          login: new_value,
          email: new_value
        )

        log_warning(duplicate)
      end

      def log_warning(user)
        message = "Found duplicate email address: #{user.email}"
        Rails.logger.info(message)
        Rollbar.warn(message) if defined?(Rollbar)
      end

    end
  end
end
