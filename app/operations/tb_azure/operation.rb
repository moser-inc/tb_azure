module TbAzure
  class Operation
    attr_accessor :success, :result
    delegate :errors, to: :result

    module ClassMethods

      # Run the operation
      #
      def run(*args, **kwargs)
        op = new
        op.result = op.call(*args, **kwargs)
        op.success = determine_success(op.result) if op.success.nil?
        op
      end

      protected

      # Analyze result for truthiness
      #
      def determine_success(result)
        if result.respond_to?(:valid?)
          result.valid?
        else
          result.present?
        end
      end
    end

    extend ClassMethods

    def success?
      success
    end

  end
end
