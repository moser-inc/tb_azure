require_dependency 'tb_azure/application_controller'

module TbAzure
  class UserSessionsController < ApplicationController
    skip_before_action :verify_authenticity_token, only: :callback
    layout 'tb_azure/user_sessions'

    def new
      redirect_url = TbAzure.context.authorization_request_url(
        TbAzure::RESOURCE,
        TbAzure.client_id,
        callback_url,
        state: encoded_state
      ).to_s
      redirect_to redirect_url, allow_other_host: true
    end

    def callback
      token_response = TbAzure.context.acquire_token_with_authorization_code(
        params[:code],
        callback_url,
        TbAzure.client,
        TbAzure::RESOURCE
      )
      handle_token_response(token_response)
    end

    def handle_token_response(token_response)
      case token_response
      when ADAL::SuccessResponse
        operation = TbAzure::Token::Login.run(token_response)
        if operation.success?
          @user = TbAzure::Token::Login.run(token_response).result
          SpudUserSession.create(@user)
          path = decoded_state.delete(:return_to) || '/'
          redirect_to "#{path}?#{URI.encode_www_form(decoded_state)}"
        else
          @error_description = operation.result.errors.full_messages.join(', ')
          render 'error', status: :bad_request
        end
      when ADAL::ErrorResponse
        @error_description = token_response.error_description
        render 'error', status: :bad_request
      end
    rescue TbAzure::Network::RequestError => e
      @error_description = e.message
      render 'error', status: :bad_request
    end

    def encoded_state
      params.reject do |k, _v|
        %i[controller action format].include?(k.to_sym)
      end.to_json
    end

    def decoded_state
      @_decoded_state ||= JSON.parse(params[:state], symbolize_names: true)
      @_decoded_state
    rescue JSON::ParserError, TypeError
      {}
    end

  end
end
