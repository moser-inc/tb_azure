module TbAzure
  module AzureAuthentication
    extend ActiveSupport::Concern

    def current_user
      return @current_user if defined?(@current_user)

      if request.headers['Authorization']
        @current_user = find_user_by_jwt
        @current_user
      else
        user = super
        if user&.should_check_azure?
          TbAzure::User::Refresh.run(
            user,
            user.azure_token
          )
        end
        user
      end
    rescue TbAzure::Network::RequestError
      current_user_session&.destroy
      raise TbCore::AccessDeniedError
    end

    private

    def jwt_auth_header
      request.headers['Authorization'].split.last if
        request.headers['Authorization'].present?
    end

    def find_user_by_jwt
      token = jwt_auth_header

      profile = TbAzure::Network.request(
        '/me',
        token,
        TbAzure.user_attribute_params
      )
      TbAzure::User::FindOrCreate.run(
        profile, access_token: token
      ).result
    end

  end
end
