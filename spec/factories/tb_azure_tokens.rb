FactoryBot.define do
  factory :tb_azure_token, class: 'TbAzure::Token' do
    spud_user
    access_token { 'access-token-12345' }
    refresh_token { 'refresh-token-67890' }
  end
end
