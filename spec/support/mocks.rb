module Mocks
  def self.azure_profile
    {
      'id' => '1234',
      'displayName' => 'user@example.com',
      'mail' => 'user@example.com',
      'givenName' => 'FirstName',
      'surname' => 'LastName',
      'jobTitle' => 'Senior Consultant'
    }
  end

  def self.azure_error
    {
      'code' => 'Request_BadRequest',
      'message' => {
        'lang' => 'en',
        'value' => 'Something went wrong!'
      }
    }
  end
end
