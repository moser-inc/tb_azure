require 'rails_helper'
require 'support/mocks'

RSpec.describe TbAzure::User::FindOrCreate, type: :operation do
  describe 'call' do
    it 'returns a user' do
      result = TbAzure::User::FindOrCreate.run(Mocks.azure_profile).result
      expect(result).to be_a(SpudUser)
      expect(result.first_name).to eq(Mocks.azure_profile['givenName'])
      expect(result.email).to eq(Mocks.azure_profile['mail'])
    end

    it 'maps additional attributes' do
      class MyMapper
        def map_attributes(profile, _token)
          { job_title: profile['jobTitle'] }
        end
      end
      TbAzure.config.mapper = 'MyMapper'
      result = TbAzure::User::FindOrCreate.run(Mocks.azure_profile).result
      expect(result.job_title).to eq('Senior Consultant')
    end

    it 'handles a duplicate email address' do
      profile = Mocks.azure_profile
      dupe_user = create(:spud_user, email: profile['mail'], login: profile['mail'])
      TbAzure::User::FindOrCreate.run(Mocks.azure_profile).result

      expect do
        dupe_user.reload
      end.to change(dupe_user, :email)
    end
  end
end
