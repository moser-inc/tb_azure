require 'rails_helper'
require 'support/mocks'

RSpec.describe TbAzure::User::Refresh, type: :operation do
  let(:user) { create(:spud_user) }

  describe 'call' do
    it 'refreshes the user attributes' do
      token = create(:tb_azure_token, spud_user: user)

      allow(token).to receive(:request) do
        profile = Mocks.azure_profile
        profile['givenName'] = 'UpdatedName'
        profile
      end

      expect do
        TbAzure::User::Refresh.run(user, token)
      end.to change(user, :first_name).to('UpdatedName')
    end

    it 'raises an error without a token' do
      expect do
        TbAzure::User::Refresh.run(user, nil)
      end.to raise_error(ArgumentError)
    end
  end
end
