require 'rails_helper'
require 'support/mocks'
require 'adal/token_response'

RSpec.describe TbAzure::Token::Refresh, type: :operation do
  let :token_response do
    ADAL::SuccessResponse.new(
      access_token: 'access-token-123',
      refresh_token: 'refresh-token-456'
    )
  end

  before :each do
    allow_any_instance_of(ADAL::AuthenticationContext).to receive(:acquire_token_with_refresh_token) do
      token_response
    end
  end

  describe 'call' do
    it 'refreshes the token' do
      azure_token = FactoryBot.create(:tb_azure_token)
      expect do
        TbAzure::Token::Refresh.run(azure_token)
      end.to change(azure_token, :access_token)
    end
  end
end
