require 'rails_helper'
require 'support/mocks'
require 'adal/token_response'

RSpec.describe TbAzure::Token::Login, type: :operation do
  let :token_response do
    ADAL::SuccessResponse.new(
      access_token: 'access-token-123',
      refresh_token: 'refresh-token-456'
    )
  end

  describe 'call' do
    it 'creates a token and user' do
      allow(TbAzure::Network).to receive(:request) { Mocks.azure_profile }
      result = TbAzure::Token::Login.run(token_response).result
      expect(result).to be_a(SpudUser)
      expect(result.azure_token).to be_a(TbAzure::Token)
    end
  end
end
