# This migration comes from tb_azure (originally 20160916143432)
class AddAzureAttributesToSpudUser < ActiveRecord::Migration[5.0]
  def change
    add_column :spud_users, :azure_principal_name, :string
    add_column :spud_users, :azure_object_id, :string
  end
end
