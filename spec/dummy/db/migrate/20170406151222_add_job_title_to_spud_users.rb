class AddJobTitleToSpudUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :spud_users, :job_title, :string
  end
end
