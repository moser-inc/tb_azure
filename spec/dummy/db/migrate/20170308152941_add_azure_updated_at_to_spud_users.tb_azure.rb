# This migration comes from tb_azure (originally 20170308145751)
class AddAzureUpdatedAtToSpudUsers < ActiveRecord::Migration[5.0]
  def change
    add_column :spud_users, :azure_updated_at, :datetime
  end
end
