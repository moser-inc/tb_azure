class ApplicationController < TbCore::ApplicationController
  protect_from_forgery with: :exception
  prepend TbAzure::AzureAuthentication
  before_action :require_user

  def hello
    render plain: 'Hello!'
  end
end
