require 'rails_helper'

module TbAzure
  RSpec.describe Token, type: :model do
    let :error do
      { 'code': 'error_code', 'message' => { 'vaue' => 'message' } }
    end

    describe '#request' do
      it 'refreshes the token and tries again' do
        first_attempt = true

        # Stub this method to raise an expiration on the first try, but not the second
        #
        allow(TbAzure::Network).to receive(:request) do
          if first_attempt
            first_attempt = false
            raise TbAzure::Network::ExpiredTokenError, error
          else
            { success: 1 }
          end
        end

        # Stub refreshing of a token
        #
        stub_request(:post, 'https://login.microsoftonline.com/ADAL-TEST-TENANT/oauth2/token')
          .to_return(
            status: 200,
            body: '{"access_token": "new-token", "refresh_token": "new-refresh-token"}',
            headers: {}
        )

        token = FactoryBot.create(:tb_azure_token)
        expect do
          token.request('/me')
        end.to change(token, :access_token)
      end
    end
  end
end
