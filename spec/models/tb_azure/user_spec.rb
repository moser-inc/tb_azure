require 'rails_helper'

module TbAzure
  RSpec.describe User, type: :model do
    let(:user) do
      build(:spud_user,
        azure_object_id: '1234',
        azure_updated_at: 5.minutes.ago
      )
    end

    describe 'azure?' do
      it 'returns true' do
        expect(user.azure?).to eq(true)
      end
      it 'returns false' do
        user.azure_object_id = nil
        expect(user.azure?).to eq(false)
      end
    end

    describe 'should_check_azure?' do
      it 'returns true' do
        expect(user.should_check_azure?).to eq(true)
      end
      it 'returns false' do
        user.azure_updated_at = 5.seconds.ago
        expect(user.should_check_azure?).to eq(false)
      end
    end
  end
end
