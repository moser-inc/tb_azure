require 'rails_helper'

RSpec.describe TbAzure::Network do

  let :access_token do
    SecureRandom.hex
  end

  let :email do
    'first.last@company.com'
  end

  describe '.request' do
    it 'returns some json' do
      stub_request(:get, 'https://graph.microsoft.com/v1.0/me')
        .to_return(status: 200, body: "{\"mail\": \"#{email}\"}", headers: {})
      result = TbAzure::Network.request('/me', access_token)
      expect(result['mail']).to eq(email)
    end

    it 'raises an error' do
      stub_request(:get, 'https://graph.microsoft.com/v1.0/me')
        .to_return(status: 200, body: '{"error": {"code": "InvalidAuthenticationToken"}}', headers: {})
      expect do
        TbAzure::Network.request('/me', access_token)
      end.to raise_error(TbAzure::Network::ExpiredTokenError)
    end
  end
end
