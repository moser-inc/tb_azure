require 'rails_helper'

RSpec.describe TbAzure::Network::Request do
  describe '#uri_for_endpoint' do
    context 'with a full url' do
      it 'returns a uri' do
        network = TbAzure::Network::Request.new(nil)
        result = network.uri_for_endpoint('https://google.com/hello')
        expect(result.to_s).to eq('https://google.com/hello')
      end
    end
    context 'with just a path' do
      it 'returns a uri with the tentant and endpoint' do
        network = TbAzure::Network::Request.new(nil)
        result = network.uri_for_endpoint('/me')
        expect(result.to_s).to eq('https://graph.microsoft.com/v1.0/me')
      end
    end
  end
end
