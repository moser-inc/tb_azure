require 'rails_helper'
require 'support/mocks'

RSpec.describe ApplicationController, type: :controller do
  before :each do
    activate_session
    current_user.update(
      azure_object_id: '1234',
      azure_principal_name: current_user.email,
      azure_updated_at: 5.minutes.ago
    )
    create(:tb_azure_token, spud_user: current_user)
  end

  describe 'current_user' do
    it 'refreshes the azure attributes' do
      allow_any_instance_of(TbAzure::Token).to receive(:request) { Mocks.azure_profile }
      expect do
        get :hello
        current_user.reload
      end.to change(current_user, :azure_updated_at)
    end

    it 'logs the user out' do
      allow_any_instance_of(TbAzure::User::Refresh).to receive(:call) do
        raise TbAzure::Network::RequestError, Mocks.azure_error
      end
      get :hello
      expect(response).to have_http_status :forbidden
    end
  end
end
