require 'rails_helper'

module TbAzure
  RSpec.describe UserSessionsController, type: :controller do
    before(:all) { ADAL::TokenResponse } # trigger code autoloading
    routes { TbAzure::Engine.routes }

    def mock_adal_auth_success
      allow_any_instance_of(ADAL::AuthenticationContext).to receive(:acquire_token_with_authorization_code) do
        ADAL::SuccessResponse.new(
          access_token: 'access-token-123',
          refresh_token: 'refresh-token-456'
        )
      end
    end

    describe 'new' do
      it 'redirects to the azure login page' do
        get :new
        expect(response).to redirect_to(
          'https://login.microsoftonline.com/ADAL-TEST-TENANT/oauth2/authorize?client_id=ADAL-TEST-CLIENT-ID&response_mode=form_post&redirect_uri=http%3A%2F%2Ftest.host%2Fazure%2Fcallback&resource=https%3A%2F%2Fgraph.microsoft.com&response_type=code&state=%7B%7D'
        )
      end
    end

    describe 'callback' do
      before(:each) do
        mock_adal_auth_success
        allow_any_instance_of(TbAzure::Token::Login).to receive(:call) do
          FactoryBot.create(:spud_user)
        end
      end

      it 'logs the user in' do
        post :callback, params: { code: 'response-code-1234' }
        expect(response).to redirect_to('/?')
      end

      it 'redirects to the specified path' do
        state = { return_to: '/some/place', hello: 'world' }
        post :callback, params: { code: 'response-code-1234', state: state.to_json }
        expect(response).to redirect_to('/some/place?hello=world')
      end
    end

    it 'shows an error' do
      allow_any_instance_of(ADAL::AuthenticationContext).to receive(:acquire_token_with_authorization_code) do
        ADAL::ErrorResponse.new
      end
      post :callback, params: { code: 'response-code-1234' }
      expect(response.status).to eq(400)
    end

    it 'shows an error' do
      mock_adal_auth_success
      allow_any_instance_of(TbAzure::Network::Request).to receive(:request) do
        raise TbAzure::Network::RequestError, 'code' => 'Request_BadRequest',
                                              'message' => { 'value' => 'Some error' }
      end
      post :callback, params: { code: 'response-code-1234' }
      expect(response.status).to eq(400)
    end

    it 'shows validation errors' do
      mock_adal_auth_success
      allow_any_instance_of(TbAzure::Token::Login).to receive(:call) do
        build(:spud_user, first_name: nil)
      end
      post :callback, params: { code: 'response-code-1234' }
      expect(response.status).to eq(400)
    end
  end
end
